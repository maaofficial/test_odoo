from odoo import models, fields, api

class TrainCity(models.Model):
    _name = 'train.city'
    _description = 'Train City'

    name = fields.Char(string='City', required=True)
    code = fields.Char(string='Code', required=True)

class TrainStation(models.Model):
    _name = 'train.station'
    _description = 'Train Station'

    name = fields.Char(string='Station', required=True)
    code = fields.Char(string='Code', required=True)
    city = fields.Many2one('res.users', string="City")
    address = fields.Text(string='Address', required=True)

class TrainTrain(models.Model):
    _name = 'train.train'
    _description = 'Train Name'

    name = fields.Char(string='Train', required=True)
    code = fields.Char(string='Code', required=True)
    capacity = fields.Integer('Capacity', default=0)
    state = fields.Selection(string='Ready', string='Not Ready', string='Maintenance', required=True)
    active = fields.Boolean('Active', default=True)
    schedule_line = fields.One2many('training.schedule', 'schedule_time')